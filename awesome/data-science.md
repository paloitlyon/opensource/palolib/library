# Datasets
## Collections of datasets
- https://paperswithcode.com/datasets
- https://huggingface.co/datasets
- https://www.openml.org/search?type=data
- https://www.kaggle.com/datasets
- https://archive.ics.uci.edu/ml/datasets.php


## Vision
- http://tc11.cvc.uab.es/datasets/SVT_1
- https://vision.cornell.edu/se3/coco-text-2/

# People to follow
## General
- Vincent Warmerdam - critical thinking on ml techniques, lots of tutorials and tricks on python for data science, fairness: [twitter](https://twitter.com/fishnets88), [linkedin](https://www.linkedin.com/in/vincentwarmerdam/), [blog](https://koaning.io/), [tutorials](https://calmcode.io/)
- Virginie Mathivet - in FR, news and opinions on AI news and services: https://www.linkedin.com/in/virginie-mathivet/

## NLP
- Vincent Warmerdam - critical thinking on ml techniques, lots of tutorials and tricks on python for data science, fairness: [twitter](https://twitter.com/fishnets88), [linkedin](https://www.linkedin.com/in/vincentwarmerdam/), [blog](https://koaning.io/), [tutorials](https://calmcode.io/)
- Phillip Vollet - High frequency and high quality content, mostly sharing links to OSS projects and NLP resources [linkedin](https://www.linkedin.com/in/philipvollet/)

# Newsletters / RSS Feeds
## Low Frequency, High Quality
- NLP: Depends on the definition: https://www.depends-on-the-definition.com/
- NLP: Explosion.ai - creators of Spacy & Prodigy: [Blog RSS feed](https://explosion.ai/feed) / [blog](https://explosion.ai/blog)
- NLP: Sebastian Ruder: [Blog RSS feed](https://ruder.io/rss/index.rss) / [Blog](https://ruder.io/)
- Ethical ML: The ML Engineer: https://ethical.institute/mle.html
