# AWESOME LIST

On this shelf, you will find some awesome list curated with love and passion.

- [Awesome DevOps](devops.md)

- [Awesome Watch](watch.md)

- [Awesome Web Development](web-development.md)

- [Awesome Python](python.md)

- [Awesome Data Science](data-science.md)
