# AWESOME LIST - WEB DEVELOPMENT

## Application

* [Responsively](https://responsively.app) : open source devtool for responsive FrontEnd development

* [Metatags](https://metatags.io/) : metatag generator

* [Smalldev](https://smalldev.tools/) : small dev tools (qrcode, lorem ipsum, test data generator, decoders, etc)

## Assets

- [SVGOMG ](https://jakearchibald.github.io/svgomg/) : SVG optimizer GUI
- [Unsplash](https://unsplash.com/) : quality and free photos

## CSS

- [Type scale](https://type-scale.com/) : Visual type scale (google fonts example)

## Documentation

- [Storybook](https://storybook.js.org/) : document UI components
- [Carbon](https://carbon.now.sh/) : create images of source code

## ORM

* [Prisma](https://www.prisma.io/) : Typescript ORM for Node.js

* [Objection](https://vincit.github.io/objection.js/) : ORM for Node.js

* [TypeORM](https://typeorm.io) :  ORM for Node.js, browser, React Native, NativeScript, Electron

## Ressources

* [French gov repo](https://code.gouv.fr/) : open source code from public french organization
