# Talks
## Python core
- James Powell - So you want to be a Python expert ? - Long tutorial that leads to very good understanding of python concepts (decorators, context managers, generators): https://www.youtube.com/watch?v=cKPlPJyQrt4

# Parallel Computing on larger than RAM datasets
- Dask
    - Aaron Richter- Parallel Processing in Python| PyData Global 2020 https://www.youtube.com/watch?v=eJyjB3cNIB0
