# AWESOME LIST - TECHNOLOGICAL WATCH

## DEVELOPMENT

* [Bits & Pieces](https://blog.bitsrc.io/) : curated knowledge for developers (web oriented)
* [FreeCodeCamp](https://www.freecodecamp.org/news/) : curated knowledge for developers
* [CSS Tricks](https://css-tricks.com/archives/) : curated knowledge on CSS
* [The Changelog](https://changelog.com/) : podcasts for developers

## DESIGN

* [UX Design](https://uxdesign.cc/) : curated article about UX design
* [Muzli](https://medium.muz.li/) : curated article about design
* [Google Design](https://design.google/) : google UXers sharing their thoughts
* [UsabilityGeek](https://usabilitygeek.com/) : curated article on UI/UX
