
### ml + js 
- [ConvNetJS: Deep Learning in your browser](https://cs.stanford.edu/people/karpathy/convnetjs/)  
- [GitHub - microsoft/onnxjs: ONNX.js: run ONNX models using JavaScript](https://github.com/microsoft/onnxjs)  
- [TensorFlow.js \| Le machine learning pour les développeurs JavaScript](https://www.tensorflow.org/js?hl=fr)  
### nlp - spacy 
- [A full spaCy pipeline and models for scientific/biomedical documents. \| PythonRepo](https://pythonrepo.com/repo/allenai-scispacy-python-natural-language-processing)  
- [explosion/floret: 🌸 fastText + Bloom embeddings for compact, full-coverage vectors with spaCy](https://github.com/explosion/floret) 
- [✨Fast Coreference Resolution in spaCy with Neural Networks \| PythonRepo](https://pythonrepo.com/repo/huggingface-neuralcoref-python-natural-language-processing) 
- [scispacy \| SpaCy models for biomedical text processing](https://allenai.github.io/scispacy/) 
- [Pipeline Conversations: Creating Tools that Spark Joy with Ines Montani](https://podcast.zenml.io/ines-montani) 
- [chartbeat-labs/textacy: NLP, before and after spaCy](https://github.com/chartbeat-labs/textacy) 
- [projects/tutorials at v3 · explosion/projects](https://github.com/explosion/projects/tree/v3/tutorials) 
- [DerwenAI/pytextrank: Python implementation of TextRank for phrase extraction and summarization of text documents](https://github.com/DerwenAI/pytextrank) 
- [jaidevd/numerizer: A Python module to convert natural language numerics into ints and floats.](https://github.com/jaidevd/numerizer) 
- [🛸 Use pretrained transformers like BERT, XLNet and GPT-2 in spaCy \| PythonRepo](https://pythonrepo.com/repo/explosion-spacy-transformers-python-natural-language-processing) 
- [KennethEnevoldsen/augmenty: Augmenty is an augmentation library based on spaCy for augmenting texts.](https://github.com/KennethEnevoldsen/augmenty) 
- [Two minutes NLP --- SpaCy cheat sheet \| by Fabio Chiusano \| NLPlanet \| Jan, 2022 \| Medium](https://medium.com/nlplanet/two-minutes-nlp-spacy-cheat-sheet-21471dac7837) 
- [pmbaumgartner/spacy-setfit-textcat](https://github.com/pmbaumgartner/spacy-setfit-textcat) 
- [projects/pipelines/ner_demo_replace at v3 · explosion/projects](https://github.com/explosion/projects/tree/v3/pipelines/ner_demo_replace) 
- [spaCy consulting & jobs · Discussion #10259 · explosion/spaCy](https://github.com/explosion/spaCy/discussions/10259) 
- [DerwenAI/pytextrank: Python implementation of TextRank for phrase extraction and summarization of text documents](https://github.com/DerwenAI/pytextrank) 
- [LeapBeyond/scrubadub_spacy: Clean personally identifiable information from dirty dirty text using spaCy.](https://github.com/LeapBeyond/scrubadub_spacy) 
- [pmbaumgartner/spacy-html-tokenizer](https://github.com/pmbaumgartner/spacy-html-tokenizer) 
- [luismond/tm2tb: Bilingual term extraction and alignment with spaCy and sentence transformers.](https://github.com/luismond/tm2tb) 
- [Classy Classification · spaCy Universe](https://spacy.io/universe/project/classyclassification) 
- [aphp/edsnlp: EDS-NLP provides a set of spaCy components to extract information from clinical notes written in French.](https://github.com/aphp/edsnlp) 
- [PacktPublishing/Mastering-spaCy: Mastering spaCy, published by Packt](https://github.com/PacktPublishing/Mastering-spaCy) 
- [SapienzaNLP/extend: Entity Disambiguation as text extraction (ACL 2022)](https://github.com/SapienzaNLP/extend) 
- [aphp/edsnlp: EDS-NLP provides a set of spaCy components to extract information from clinical notes written in French.](https://github.com/aphp/edsnlp) 
- [Crosslingual Coreference · spaCy Universe](https://spacy.io/universe/project/crosslingualcoreference) 
- [GitHub - jboynyc/textnets: Text analysis with networks.](https://github.com/jboynyc/textnets) 

### nlp - topic modeling & keyphrase extraction 
- [MilaNLProc/contextualized-topic-models: A python package to run contextualized topic modeling. CTMs combine contextualized embeddings (e.g., BERT) with topic models to get coherent topics. Published at EACL and ACL 2021.](https://github.com/MilaNLProc/contextualized-topic-models) 
- [BERTopic.ipynb - Colaboratory](https://colab.research.google.com/drive/1FieRA9fLdkQEGDIMYl0I3MCjSUKVF8C-?usp=sharing#scrollTo=SoMc1W-x7-b5) 
- [MaartenGr/BERTopic: Leveraging BERT and c-TF-IDF to create easily interpretable topics.](https://github.com/MaartenGr/BERTopic) 
- [Tutorial (v2.2.0): Zero-shot Cross-Lingual + Visualizations - Colaboratory](https://colab.research.google.com/drive/1bfWUYEypULFk_4Tfff-Pb_n7-tSjEe9v?usp=sharing#scrollTo=mFhhsliMtDXJ) 
- [sahyagiri/DistinctKeywords: semantically distinct key phrase extraction using hilbert hashes.](https://github.com/sahyagiri/DistinctKeywords) 
- [MaartenGr/KeyBERT: Minimal keyword extraction with BERT](https://github.com/MaartenGr/KeyBERT) 
- [ddangelov/Top2Vec: Top2Vec learns jointly embedded topic, document and word vectors.](https://github.com/ddangelov/Top2Vec) 
- [(4) Publier \| LinkedIn](https://www.linkedin.com/posts/prithivirajdamodaran_lsa-lda-top2vec-activity-6925757992227250176-Lga4/?utm_source=linkedin_share&utm_medium=member_desktop_web) 
- [boudinfl/pke: Python Keyphrase Extraction module](https://github.com/boudinfl/pke) 

### nlp - entity & relation extraction 
- [Knowledge graph completion with PyKEEN and Neo4j \| by Tomaz Bratanic \| Dec, 2021 \| Towards Data Science](https://towardsdatascience.com/knowledge-graph-completion-with-pykeen-and-neo4j-6bca734edf43) 
- [pykeen/pykeen: 🤖 A Python library for learning and evaluating knowledge graph embeddings](https://github.com/pykeen/pykeen) 
- [:id: A python library for accurate and scalable fuzzy matching, record deduplication and entity-resolution. \| PythonRepo](https://pythonrepo.com/repo/dedupeio-dedupe-python-natural-language-processing) 
- [Fuzzy String Matching in Python \| PythonRepo](https://pythonrepo.com/repo/seatgeek-fuzzywuzzy-python-natural-language-processing) 
- [Named-entity recognition using neural networks. Easy-to-use and state-of-the-art results. \| PythonRepo](https://pythonrepo.com/repo/Franck-Dernoncourt-NeuroNER-python-natural-language-processing) 
- [MAIF/melusine: Melusine is a high-level library for emails classification and feature extraction \"dédiée aux courriels français\".](https://github.com/MAIF/melusine) 
- [erre-quadro/spikex: SpikeX - SpaCy Pipes for Knowledge Extraction](https://github.com/erre-quadro/spikex) 
- [Web mining module for Python, with tools for scraping, natural language processing, machine learning, network analysis and visualization. \| PythonRepo](https://pythonrepo.com/repo/clips-pattern-python-natural-language-processing) 
- [roomylee/awesome-relation-extraction: 📖 A curated list of awesome resources dedicated to Relation Extraction, one of the most important tasks in Natural Language Processing (NLP).](https://github.com/roomylee/awesome-relation-extraction) 
- [paulrinckens/timexy: A spaCy custom component that extracts and normalizes temporal expressions](https://github.com/paulrinckens/timexy) 
- [Snips Python library to extract meaning from text \| PythonRepo](https://pythonrepo.com/repo/snipsco-snips-nlu-python-natural-language-processing) 
- [Annotate records --- Rubrix 0.12 documentation](https://rubrix.readthedocs.io/en/stable/reference/webapp/annotate_records.html) 
- [booknlp/booknlp: BookNLP, a natural language processing pipeline for books](https://github.com/booknlp/booknlp) 
- [kabirkhan/recon: Recon NER, Debug and correct annotated Named Entity Recognition (NER) data for inconsistencies and get insights on improving the quality of your data.](https://github.com/kabirkhan/recon) 
- [msg-systems/coreferee: Coreference resolution for English, French, German and Polish, optimised for limited training data and easily extensible for further languages](https://github.com/msg-systems/coreferee) 
- [Extract knowledge from text: End-to-end information extraction pipeline with spaCy and Neo4j \| by Tomaz Bratanic \| May, 2022 \| Towards Data Science](https://towardsdatascience.com/extract-knowledge-from-text-end-to-end-information-extraction-pipeline-with-spacy-and-neo4j-502b2b1e0754#6a9e-7c86c8802227) 
- [Extract knowledge from text: End-to-end information extraction pipeline with spaCy and Neo4j \| by Tomaz Bratanic \| May, 2022 \| Towards Data Science](https://towardsdatascience.com/extract-knowledge-from-text-end-to-end-information-extraction-pipeline-with-spacy-and-neo4j-502b2b1e0754) 
- [Advanced PII detection and anonymization with Hugging Face Transformers and Amazon SageMaker](https://www.philschmid.de/pii-huggingface-sagemaker) 

### nlp - ontologies & knowledge 
- [jibalamy / owlready2 --- Bitbucket](https://bitbucket.org/jibalamy/owlready2/src/master/) 

### nlp - general 
- [Jordan Boyd-Graber: Courses](http://users.umiacs.umd.edu/~jbg/static/courses.html) 
- [Graph4nlp is the library for the easy use of Graph Neural Networks for NLP \| PythonRepo](https://pythonrepo.com/repo/graph4ai-graph4nlp-python-natural-language-processing) 
- [A framework for training and evaluating AI models on a variety of openly available dialogue datasets. \| PythonRepo](https://pythonrepo.com/repo/facebookresearch-ParlAI-python-natural-language-processing) 
- [natural_language_processing.ipynb - Colaboratory](https://colab.research.google.com/github/khuyentran1401/Efficient_Python_tricks_and_tools_for_data_scientists/blob/master/Chapter5/natural_language_processing.ipynb) 
- [Gramformer - a Hugging Face Space by prithivida](https://huggingface.co/spaces/prithivida/Gramformer) 
- [GitHub - booknlp/booknlp: BookNLP, a natural language processing pipeline for books](https://github.com/booknlp/booknlp) 
- [:mag: Ambar: Document Search Engine \| PythonRepo](https://pythonrepo.com/repo/RD17-ambar-python-organization) 
- [Beautiful visualizations of how language differs among document types. \| PythonRepo](https://pythonrepo.com/repo/JasonKessler-scattertext-python-natural-language-processing) 
- [MaartenGr/BERTopic: Leveraging BERT and c-TF-IDF to create easily interpretable topics.](https://github.com/MaartenGr/BERTopic) 
- [BERT Goes Shopping: Comparing Distributional Models for Product Representations (Paper Walkthrough) - YouTube](https://www.youtube.com/watch?v=sz-WGz8gg98) 
- [Tracking Progress in Natural Language Processing \| NLP-progress](https://nlpprogress.com/) 
- [TextAttack 🐙 is a Python framework for adversarial attacks, data augmentation, and model training in NLP \| PythonRepo](https://pythonrepo.com/repo/QData-TextAttack-python-natural-language-processing) 
- [How To Speed Up Deep Learning Inference For NLP Transformers](https://nlpcloud.io/how-to-speed-up-deep-learning-nlp-transformers-inference.html?utm_source=reddit&utm_campaign=ehyiek56-ed8e-11eb-ba80-5242ac130007) 
- [txtai executes machine-learning workflows to transform data and build AI-powered semantic search applications. \| PythonRepo](https://pythonrepo.com/repo/neuml-txtai-python-indexing-and-performing-search-queries-on-data) 
- [deepset-ai/haystack: Haystack is an open source NLP framework that leverages Transformer models. It enables developers to implement production-ready neural search, question answering, semantic document search and summarization for a wide range of applications.](https://github.com/deepset-ai/haystack) 
- [deepset-ai/FARM: Fast & easy transfer learning for NLP. Harvesting language models for the industry. Focus on Question Answering.](https://github.com/deepset-ai/FARM) 
- [How to Automate Information Extraction with Haystack-based Question Answering \| deepset](https://www.deepset.ai/blog/automating-information-extraction-with-question-answering) 
- [Worldwide Trillions of Dollars in value are lost in PDFs \| Real-world Machine Learning](https://dramsch.net/posts/worldwide-trillions-of-dollars-in-value-are-lost-in-pdfs/) 
- [thomasthiebaud/spacy-fastlang: Language detection using Spacy and Fasttext](https://github.com/thomasthiebaud/spacy-fastlang) 
- [Python wrapper for Stanford CoreNLP tools v3.4.1 \| PythonRepo](https://pythonrepo.com/repo/dasmith-stanford-corenlp-python-python-natural-language-processing) 
- [A python framework to transform natural language questions to queries in a database query language. \| PythonRepo](https://pythonrepo.com/repo/machinalis-quepy-python-natural-language-processing) 
- [axa-group/Parsr: Transforms PDF, Documents and Images into Enriched Structured Data](https://github.com/axa-group/Parsr) 
- [Welcome to Rubrix --- Rubrix 0.10 documentation](https://rubrix.readthedocs.io/en/stable/index.html) 
- [stanford-futuredata/ColBERT: ColBERT: state-of-the-art neural search (SIGIR\'20, TACL\'21, NeurIPS\'21)](https://github.com/stanford-futuredata/ColBERT) 
- [nlp-with-transformers/notebooks: Jupyter notebooks for the Natural Language Processing with Transformers book](https://github.com/nlp-with-transformers/notebooks) 
- [Building Scalable, Explainable, and Adaptive NLP Models with Retrieval \| SAIL Blog](https://ai.stanford.edu/blog/retrieval-based-NLP/) 
- [PrithivirajDamodaran/Alt-ZSC: Alternate Implementation for Zero Shot Text Classification: Instead of reframing NLI/XNLI, this reframes the text backbone of CLIP models to do ZSC. Hence, can be lightweight + supports more languages without trading-off accuracy. (Super simple, a 10th-grader could totally write this but since no 10th-grader did, I did) - Prithivi Da](https://github.com/PrithivirajDamodaran/Alt-ZSC) 
- [ivan-bilan/The-NLP-Pandect: A comprehensive reference for all topics related to Natural Language Processing](https://github.com/ivan-bilan/The-NLP-Pandect) 
- [GitHub - yueyu1030/COSINE: This is the code for our paper \`Fine-Tuning Pre-trained Language Model with Weak Supervision: A Contrastive-Regularized Self-Training Approach\' (In Proc. of NAACL-HLT 2021).](https://github.com/yueyu1030/COSINE) 
- [GitHub - MilaNLProc/xlm-emo](https://github.com/MilaNLProc/xlm-emo) 
- [Haystack Docs](https://haystack.deepset.ai/docs/intromd) 
- [DLG4NLP AAAI22 - YouTube](https://www.youtube.com/watch?v=QZEetG6YX-Y) 

### nlp - viz 
- [Text Visualization Browser](https://textvis.lnu.se/) 
- [Named Entity Recognition](https://dash.gallery/named-entity-recognition/) 
- [koaning/cluestar: Gain clues from clustering!](https://github.com/koaning/cluestar) 

### nlp - chatbots 
- [ChatterBot is a machine learning, conversational dialog engine for creating chat bots \| PythonRepo](https://pythonrepo.com/repo/gunthercox-ChatterBot-python-natural-language-processing) 
- [AnjanaRita/converse: Conversational text Analysis using various NLP techniques](https://github.com/AnjanaRita/converse) 
- [Custom SpaCy 3.0 models in Rasa \| The Rasa Blog \| Rasa](https://rasa.com/blog/custom-spacy-3-0-models-in-rasa/) 
- [Implementing a Custom Intent Classification Model with Rasa \| by Popescu Daniel \| MantisNLP \| Feb, 2022 \| Medium](https://medium.com/mantisnlp/implementing-a-custom-intent-classification-model-with-rasa-5ab4283b5b14) 
- [MLOps for Conversational AI with Rasa, DVC, and CML (Part I) \| by Matthew Upson \| MantisNLP \| Medium](https://medium.com/mantisnlp/mlops-for-conversational-ai-with-rasa-dvc-and-cml-part-i-beec756e8e7f) 
- [RASA Series\' Articles - DEV Community](https://dev.to/petr7555/series/11401) 

### nlp - xai 
- [A Python package implementing a new model for text classification with visualization tools for Explainable AI :octocat: \| PythonRepo](https://pythonrepo.com/repo/sergioburdisso-pyss3-python-natural-language-processing) 
- [ESVP - Expliquer S\'il Vous Plaît - a Hugging Face Space by prithivida](https://huggingface.co/spaces/prithivida/ESVP) 

### nlp - pretraining 
- [Pretraining Language Models: Quality Over Quantity? - inovex GmbH](https://www.inovex.de/de/blog/pretraining-language-models/) 
- [nlp-with-transformers/notebooks: Jupyter notebooks for the Natural Language Processing with Transformers book](https://github.com/nlp-with-transformers/notebooks) 
- [SentenceTransformers Documentation --- Sentence-Transformers documentation](https://www.sbert.net/) 
- [GPT-J \| GPT-3 Demo](https://gpt3demo.com/apps/gpt-j-6b) 
- [EleutherAI - text generation testing UI](https://6b.eleuther.ai/) 
- [🏷️ Label your data to fine-tune a classifier with Hugging Face --- Rubrix master documentation](https://rubrix.readthedocs.io/en/master/tutorials/01-labeling-finetuning.html) 

### nlp - blogs 
- [OpenAI GPT-3 Text Embeddings - Really a new state-of-the-art in dense text embeddings? \| by Nils Reimers \| Jan, 2022 \| Medium](https://medium.com/@nils_reimers/openai-gpt-3-text-embeddings-really-a-new-state-of-the-art-in-dense-text-embeddings-6571fe3ec9d9) 
- [Publier \| LinkedIn](https://www.linkedin.com/posts/prithivirajdamodaran_simple-trick-for-great-sentence-embeddings-activity-6898297029303386112-Bnak/) 

### nlp - text generation 
- [mutate](https://www.linkedin.com/posts/logeshkumaru_nlp-activity-6903581387795091456-4XcJ/) 
- [infinitylogesh/mutate: A library to synthesize text datasets using Large Language Models (LLM)](https://github.com/infinitylogesh/mutate) 
- [Home](https://awesome-panel.org/home) 

### nlp - neural search 
- [An easier way to build neural search on the cloud \| PythonRepo](https://pythonrepo.com/repo/jina-ai-jina-python-natural-language-processing) 
- [Haystack Docs](https://haystack.deepset.ai/docs/intromd) 
- [Jina AI \| Jina AI is a Neural Search Company](https://jina.ai/) 
- [Prithivi Da - ANN 1/3](https://www.linkedin.com/posts/prithivirajdamodaran_deepscoop-activity-6926052261471113216-qSgP/?utm_source=linkedin_share&utm_medium=member_desktop_web) 
- [Prithivi Da - ANN 2/3](https://www.linkedin.com/posts/prithivirajdamodaran_deepscoop-vectorsearch-ann-activity-6926765161223499776-CU2q/?utm_source=linkedin_share&utm_medium=ios_app) 
- [Prithivi Da - ANN 3/3](https://www.linkedin.com/posts/prithivirajdamodaran_deepscoop-vectorsearch-machinelearning-activity-6927162860947058688-qIDL/?utm_source=linkedin_share&utm_medium=member_desktop_web) 
- [Prithivi Da = ANN Part 4](https://www.linkedin.com/posts/prithivirajdamodaran_deepscoop-vectorsearch-ann-activity-6927508377916870656-IDC5/?utm_source=linkedin_share&utm_medium=member_desktop_web) 
- [weaviate-examples/question-answering-application-with-weaviate-workshop at main · semi-technologies/weaviate-examples](https://github.com/semi-technologies/weaviate-examples/tree/main/question-answering-application-with-weaviate-workshop) 
- [MaartenGr/PolyFuzz: Fuzzy string matching, grouping, and evaluation.](https://github.com/MaartenGr/PolyFuzz) 
- [GitHub - zinclabs/zinc: ZincSearch. A lightweight alternative to elasticsearch that requires minimal resources, written in Go.](https://github.com/zinclabs/zinc?s=09) 

### nlp - embeddings 
- [clips/mfaq · Hugging Face](https://huggingface.co/clips/mfaq) 
- [SentenceTransformers Documentation --- Sentence-Transformers documentation](https://www.sbert.net/index.html) 
- [oborchers/Fast_Sentence_Embeddings: Compute Sentence Embeddings Fast!](https://github.com/oborchers/Fast_Sentence_Embeddings) 
- [JODIE: Predicting Dynamic Embedding Trajectory in Temporal Interaction Networks](https://snap.stanford.edu/jodie/) 

### active learning 
- [Overview of Active Learning for Deep Learning](https://jacobgil.github.io/deeplearning/activelearning) 
### anomaly detection 
- [openvinotoolkit/anomalib: An anomaly detection library comprising state-of-the-art algorithms and features such as experiment management, hyper-parameter optimization, and edge inference.](https://github.com/openvinotoolkit/anomalib) 
- [(5) Publier \| LinkedIn](https://www.linkedin.com/posts/prithivirajdamodaran_hidden-anomaly-detection-with-weighted-activity-6903573446056386560-RFW0/) 

### applications 
- [A modular, high performance, headless e-commerce platform built with Python, GraphQL, Django, and React.](https://pythonrepo.com/repo/mirumee-saleor-python-e-commerce-and-payments) 
- [DeepGraphLearning/torchdrug: A powerful and flexible machine learning platform for drug discovery](https://github.com/DeepGraphLearning/torchdrug) 
- [Genetic Programming in Python, with a scikit-learn inspired API \| PythonRepo](https://pythonrepo.com/repo/trevorstephens-gplearn-python-deep-learning) 
- [Neural Architecture Search (NAS): basic principles and different approaches \| AI Summer](https://theaisummer.com/neural-architecture-search/) 

### annotation 
- [Welcome](https://inception-project.github.io/) 
- [doccano/auto-labeling-pipeline: doccano auto labeling pipeline helps doccano to annotate a document automatically.](https://github.com/doccano/auto-labeling-pipeline) 
- [Prodigy · An annotation tool for AI, Machine Learning & NLP](https://prodi.gy/) 

### chatbots development companies 
- [FCB.ai: Performance-driven AI Solutions](https://www.fcb.ai/fr/) 

### clustering 
- [UMAP: Uniform Manifold Approximation and Projection for Dimension Reduction --- umap 0.5 documentation](https://umap-learn.readthedocs.io/en/latest/) 
- [The hdbscan Clustering Library --- hdbscan 0.8.1 documentation](https://hdbscan.readthedocs.io/en/latest/index.html) 
- [koaning/cluestar: Gain clues from clustering!](https://github.com/koaning/cluestar) 

### courses 
- [NEWSLETTER depends-on-the-definition](https://mailchi.mp/8822ca758ee8/gwitlrbiwz) 
- [Home - Made With ML](https://madewithml.com/) 
- [jakevdp/PythonDataScienceHandbook: Python Data Science Handbook: full text in Jupyter Notebooks](https://github.com/jakevdp/PythonDataScienceHandbook) 
- [Natural Language Processing (NLP) for Semantic Search \| Pinecone](https://www.pinecone.io/learn/nlp/) 
- [James Briggs - YouTube](https://www.youtube.com/c/jamesbriggs) 
- [Publier \| LinkedIn](https://www.linkedin.com/posts/damienbenveniste_machinelearning-design-coding-activity-6925836018650677249-6-of/) 
- [MLU-Explain](https://mlu-explain.github.io/) 
- [GitHub - InterviewReady/system-design-resources: These are the best resources for System Design on the Internet](https://github.com/InterviewReady/system-design-resources) 
- [Statistics with Julia: Fundamentals for Data Science, Machine Learning and Artificial Intelligence](https://statisticswithjulia.org/tutorials/) 

### data engineering 
- [Data Lakes: The Definitive Guide \| LakeFS](https://lakefs.io/data-lakes/) 

### datasets 
- [Machine Learning Datasets \| Papers With Code](https://paperswithcode.com/datasets) 
- [Jeux de données de la plateforme data de la Métropole de Lyon - data.grandlyon.com](https://data.grandlyon.com/jeux-de-donnees) 
- [OpenML Home](https://www.openml.org/home) 
- [lex_glue · Datasets at Hugging Face](https://huggingface.co/datasets/lex_glue) 
- [Stanford Large Network Dataset Collection](http://snap.stanford.edu/data/index.html) 

### dataviz 
- [HiPlot makes understanding high dimensional data easy \| PythonRepo](https://pythonrepo.com/repo/facebookresearch-hiplot-python-data-validation) 
- [I prefer to use Panel for my data apps. Here is why. \| by Marc Skov Madsen \| Feb, 2022 \| Medium](https://medium.com/@marcskovmadsen/i-prefer-to-use-panel-for-my-data-apps-here-is-why-1ff5d2b98e8f) 
- [awesome-panel](https://awesome-panel.org/volume_profile_analysis) 
- [app · Streamlit](https://awesome-streamlit.org/) 
- [vizzuhq/ipyvizzu: ipyvizzu is the Jupyter Notebook integration of Vizzu.](https://github.com/vizzuhq/ipyvizzu) 
- [Dataviz Inspiration \| Hundreds of chart examples](https://www.dataviz-inspiration.com/) 

### data/ml-ops 
- [ClearML - Auto-Magical Suite of tools to streamline your ML workflow. Experiment Manager, MLOps and Data-Management \| PythonRepo](https://pythonrepo.com/repo/allegroai-clearml-python-machine-learning) 
- [evidentlyai/evidently: Interactive reports to analyze machine learning models during validation or production monitoring.](https://github.com/evidentlyai/evidently) 
- [whylabs/whylogs: Profile and monitor your ML data pipeline end-to-end , Join us in slack @ http://join.slack.whylabs.ai/](https://github.com/whylabs/whylogs) 
- [SeldonIO/alibi-detect: Algorithms for outlier, adversarial and drift detection](https://github.com/SeldonIO/alibi-detect) 
- [(JMLR\'19) A Python Toolbox for Scalable Outlier Detection (Anomaly Detection) \| PythonRepo](https://pythonrepo.com/repo/yzhao062-pyod-python-deep-learning) 
- [sodadata/soda-sql: Data profiling, testing, and monitoring for SQL accessible data.](https://github.com/sodadata/soda-sql) 
- [GitHub - re-data/re-data: re_data - fix data issues before your users & CEO would discover them 😊](https://github.com/re-data/re-data) 
- [jina-ai/docarray: The data structure for unstructured data](https://github.com/jina-ai/docarray) 
- [Data Science with Dagstermill \| Dagster\"](https://docs.dagster.io/integrations/dagstermill) 
- [Publier \| LinkedIn](https://www.linkedin.com/posts/ram-seshadri-nyc-nj_autovimlfeaturewiz-activity-6901331035796422656-KRW5/) 
- [GitHub - ploomber/ploomber: The fastest ⚡️ way to build data pipelines. Develop iteratively, deploy anywhere. ☁️](https://github.com/ploomber/ploomber) 
- [Deploy a ML inference service on a budget in less than 10 lines of code. \| PythonRepo](https://pythonrepo.com/repo/ebhy-budgetml) 
- [Nouvelles approches de l'IA : Data-Centric AI & MLOps \| by Akilhoussen Onaly \| Mar, 2022 \| Medium](https://onaly.medium.com/nouvelles-approches-de-lia-data-centric-ai-mlops-3523a216bb1) 
- [MLOps - Machine Learning Operations](https://valohai.com/mlops/?utm_campaign=LinkedIn%20test%20-%20LinkedIn%20test%20-%203%20vs%206%20accounts%20performance%20for%202%20weeks&utm_content=201107215&utm_medium=social&utm_source=linkedin&hss_channel=lis-0S3KQ0evj8) 
- [replicate/cog: Containers for machine learning](https://github.com/replicate/cog) 
- [jobergum/browser-ml-inference: Edge Inference in Browser with Transformer NLP model](https://github.com/jobergum/browser-ml-inference) 
- [NannyML/nannyml: Detecting silent model failure. NannyML estimates performance with an algorithm called Confidence-based Performance estimation (CBPE), developed by core contributors. It is the only open-source algorithm capable of fully capturing the impact of data drift on performance.](https://github.com/NannyML/nannyml) 
- [GitHub - iterative/mlem: 🐶 Version and deploy your ML models following GitOps principles](https://github.com/iterative/mlem) 

### devops 
- [GitLab CI CD Tutorial for Beginners \[Crash Course\] - YouTube](https://www.youtube.com/watch?v=qP8kir2GUgo) 
- [What is SRE \| Tasks and Responsibilities of an SRE \| SRE vs DevOps - YouTube](https://www.youtube.com/watch?v=OnK4IKgLl24) 
- [Kubernetes Tutorial for Beginners \[FULL COURSE in 4 Hours\] - YouTube](https://www.youtube.com/watch?v=X48VuDVv0do) 
- [What is DevOps? REALLY understand it \| DevOps vs SRE - YouTube](https://www.youtube.com/watch?v=0yWAtQ6wYNM) 

### experiment tracking 
- [aimhubio/aim: Aim --- an easy-to-use and performant open-source experiment tracker.](https://github.com/aimhubio/aim) 
- [Accelerated deep learning R&D \| PythonRepo](https://pythonrepo.com/repo/catalyst-team-catalyst-python-deep-learning) 

### graphs 
- [Graph Neural Networks for Novice Math Fanatics](https://rish16.notion.site/Graph-Neural-Networks-for-Novice-Math-Fanatics-c51b922a595b4efd8647788475461d57) 

### graph db 
- [Neo4j Fundamentals \| Free Neo4j Courses from GraphAcademy](https://graphacademy.neo4j.com/courses/neo4j-fundamentals) 

### ml system design 
- [Amazon.com: Designing Machine Learning Systems: An Iterative Process for Production-Ready Applications: 9781098107963: Huyen, Chip: Books](https://www.amazon.com/Designing-Machine-Learning-Systems-Production-Ready/dp/1098107969) 

### ml tooling 
- [Ludwig is a toolbox that allows to train and evaluate deep learning models without the need to write code. \| PythonRepo](https://pythonrepo.com/repo/uber-ludwig-python-deep-learning) 
- [AstraZeneca/rexmex: A general purpose recommender metrics library for fair evaluation.](https://github.com/AstraZeneca/rexmex/) 
- [Data Distribution Shifts and Monitoring](https://huyenchip.com/2022/02/07/data-distribution-shifts-and-monitoring.html) 
- [online-ml/river: 🌊 Online machine learning in Python](https://github.com/online-ml/river) 

### python 
- [Scalene: a high-performance, high-precision CPU and memory profiler for Python \| PythonRepo](https://pythonrepo.com/repo/emeryberger-scalene-python-monitoring) 
- [GitHub - joerick/pyinstrument: 🚴 Call stack profiler for Python. Shows you why your code is slow!](https://github.com/joerick/pyinstrument) 
- [GitHub - tebelorg/RPA-Python: Python package for doing RPA](https://github.com/tebelorg/RPA-Python) 
- [Calendar heatmaps from Pandas time series data \| PythonRepo](https://pythonrepo.com/repo/martijnvermaat-calmap) 
- [GitHub - mvrozanti/dte: 🕟 date and time processing language](https://github.com/mvrozanti/dte) 
- [Streamz --- Streamz 0.6.3 documentation](https://streamz.readthedocs.io/en/latest/) 
- [ml-tooling/opyrator: 🪄 Turns your machine learning code into microservices with web API, interactive GUI, and more.](https://github.com/ml-tooling/opyrator) 
- [Turns your machine learning code into microservices with web API, interactive GUI, and more. \| PythonRepo](https://pythonrepo.com/repo/ml-tooling-opyrator-python-machine-learning) 
- [chiphuyen/python-is-cool: Cool Python features for machine learning that I used to be too afraid to use. Will be updated as I have more time / learn more.](https://github.com/chiphuyen/python-is-cool) 

### recommenders 
- [AstraZeneca/rexmex: A general purpose recommender metrics library for fair evaluation.](https://github.com/AstraZeneca/rexmex) 

### rgpd 
- [https://lincnil.github.io/Guide-RGPD-du-developpeur/](https://lincnil.github.io/Guide-RGPD-du-developpeur/) 
### starting a data stack 
- [The Baseline Data Stack - Going Beyond The Modern Data Stack - Part 1](https://seattledataguy.substack.com/p/the-baseline-datastack-going-beyond?s=w) 

### text-to-speech & speech-to-text 
- [coqui-ai/TTS: 🐸💬 - a deep learning toolkit for Text-to-Speech, battle-tested in research and production](https://github.com/coqui-ai/TTS) 

### timeseries 
- [timeseriesAI/tsai: Time series Timeseries Deep Learning Machine Learning Pytorch fastai \| State-of-the-art Deep Learning library for Time Series and Sequences in Pytorch / fastai](https://github.com/timeseriesAI/tsai) 
- [Automatic extraction of relevant features from time series: \| PythonRepo](https://pythonrepo.com/repo/blue-yonder-tsfresh-python-feature-engineering) 

### vision 
- [app.py · Vijish/Crop-CLIP at main](https://huggingface.co/spaces/Vijish/Crop-CLIP/blob/main/app.py) 
- [GitHub - open-mmlab/mmocr: OpenMMLab Text Detection, Recognition and Understanding Toolbox](https://github.com/open-mmlab/mmocr) 
- [FrankMocap: A Strong and Easy-to-use Single View 3D Hand+Body Pose Estimator \| PythonRepo](https://pythonrepo.com/repo/facebookresearch-frankmocap-python-deep-learning) 
- [microsoft/Swin-Transformer: This is an official implementation for \"Swin Transformer: Hierarchical Vision Transformer using Shifted Windows\".](https://github.com/microsoft/Swin-Transformer) 
- [google-research-datasets/Objectron: Objectron is a dataset of short, object-centric video clips. In addition, the videos also contain AR session metadata including camera poses, sparse point-clouds and planes. In each video, the camera moves around and above the object and captures it from different views. Each object is annotated with a 3D bounding box. The 3D bounding box describes the object's position, orientation, and dimensions. The dataset contains about 15K annotated video clips and 4M annotated images in the following categories: bikes, books, bottles, cameras, cereal boxes, chairs, cups, laptops, and shoes](https://github.com/google-research-datasets/Objectron/) 
- [Object detection and instance segmentation toolkit based on PaddlePaddle. \| PythonRepo](https://pythonrepo.com/repo/PaddlePaddle-PaddleDetection-python-deep-learning) 
- [Publier \| LinkedIn](https://www.linkedin.com/posts/prithivirajdamodaran_captionthis-multimodalmodelling-activity-6886332355632615424-Di6Y/) 
- [Publier \| LinkedIn](https://www.linkedin.com/posts/prithivirajdamodaran_multimodalmodelling-activity-6886883180582989824-qhgx/) 
- [(6) Publier \| LinkedIn](https://www.linkedin.com/posts/omarsanseviero_lets-learn-about-zero-shot-image-classification-activity-6909235441502199808-ZrvL/) 
- [Detectron2 is FAIR\'s next-generation platform for object detection and segmentation. \| PythonRepo](https://pythonrepo.com/repo/facebookresearch-detectron2-python-deep-learning) 
- [PrithivirajDamodaran/ZSIC: Zero Shot Image Classification but more, Supports Multilingual labelling and a variety of CNN based models for a vision backbone by using OpenAI CLIP for \$ conscious uses (Of course, a 10th-grader could totally write this but since no 10th-grader did, just wrote it) - Prithivi Da](https://github.com/PrithivirajDamodaran/ZSIC) 
- [google-research/pix2seq: Pix2Seq - A general framework for turning RGB pixels into semantically meaningful sequences](https://github.com/google-research/pix2seq) 

### xai 
- [A collection of research papers and software related to explainability in graph machine learning. \| PythonRepo](https://pythonrepo.com/repo/AstraZeneca-awesome-explainable-graph-reasoning-python-deep-learning-model-explanation) 
- [MAIF/shapash: 🔅 Shapash makes Machine Learning models transparent and understandable by everyone](https://github.com/MAIF/shapash) 
- [dreji18/Fairness-in-AI: Detecting Bias and ensuring Fairness in AI solutions](https://github.com/dreji18/Fairness-in-AI) 
- [PAIR-code/lit: The Language Interpretability Tool: Interactively analyze NLP models for model understanding in an extensible and framework agnostic interface.](https://github.com/pair-code/lit) 
- [GitHub - MAIF/shapash: 🔅 Shapash makes Machine Learning models transparent and understandable by everyone](https://github.com/MAIF/shapash) 
- [Xplique](https://deel-ai.github.io/xplique/) 

### misc 
- [ATLAS - Productive Synergies](https://atlas.productive-synergies.com/) 
- [What a Good Research Paper \"Looks\" Like \[Communication\] - YouTube](https://www.youtube.com/watch?v=JRg5bx_iecQ) 
- [Connected Papers \| Find and explore academic papers](https://www.connectedpapers.com/) 
- [Using Self-Organizing Maps to solve the Traveling Salesman Problem](https://diego.codes/post/som-tsp/) 
- [kathrinse/TabSurvey: Experiments on Tabular Data Models](https://github.com/kathrinse/tabsurvey) 
- [JasonKessler/scattertext: Beautiful visualizations of how language differs among document types.](https://github.com/JasonKessler/scattertext) 
- [Publier \| LinkedIn](https://www.linkedin.com/posts/alice-sh-wong-12181845_datascience-machinelearning-activity-6910953271243247616-ga9q/?utm_source=linkedin_share&utm_medium=member_desktop_web) 
- [Advanced exploratory data analysis (EDA) with Python \| by Michael Notter \| EPFL Extension School \| Feb, 2022 \| Medium](https://medium.com/epfl-extension-school/advanced-exploratory-data-analysis-eda-with-python-536fa83c578a) 
- [koaning/cluestar: Gain clues from clustering!](https://github.com/koaning/cluestar) 
- [GAM Changer](https://interpret.ml/gam-changer/) 
- [nathanhubens/fasterai: FasterAI: Prune and Distill your models with FastAI and PyTorch](https://github.com/nathanhubens/fasterai) 
- [Better Images of AI](https://betterimagesofai.org/images) 
