# AWESOME LIST - DEVOPS

## Application

* [Readme](https://readme.so/) : easy and fast Readme editor

## Architecture

- [Excalidraw](https://excalidraw.com/) : diagramming app
- [Diagrams](https://www.diagrams.net/) : diagramming app (aws services focused)

## Suggested reading

* [The architecture of Open Source Applications](http://aosabook.org/en/index.html?fbclid=IwAR2PS3PiogALUuSAMVm7Kz9POZAnVolB5Yybj03M5LCK0nMncIlRks0Q0R8) : learn from others mistakes


