# NGINX

## Source used for this documentation

[The NGINX HandBook - FreeCodeCamp](https://www.freecodecamp.org/news/the-nginx-handbook/#introduction-to-nginx)

## Notes

### General

* Test the NGINX `.conf` file

```bash
sudo nginx -t
```

* Restart NGINX

```bash
sudo systemctl restart nginx
```
* Dispatch a reload

```bash
sudo nginx -s reload
```
The `-s` option is used to dispatch signals like : `stop`, `quit`, `reload` and `reopen`

### Directives
* Simple Directives 
```nginx
error_log  /var/log/nginx/error.log warn;
```
* Block Directives
```nginx
events {
  worker_connections  1024;
}
```
Block directives containing other directives are called *context*.
There are 4 core contexts : 
* `events` : global configurations for handling requests on general level.
-> **Unique** in a `.conf` file
* `http` : handle http and https requests.
-> **Unique** in a `.conf` file
* `server` : nested inside `http` context, used to configure a virtual server within a single host. Each `server` context is considered as a virtual host.
* `main` : is the configuration file itself. Anything outside of another context is on the `main` context.

[List of directives](https://nginx.org/en/docs/dirindex.html) from the official doc.

### Server

Dispatch request to the right server thanks to `listen` ...
```nginx
http {
  server {
      listen       80;
      server_name  localhost;
      return 200 "hello from port 80!\n";
  }
  server {
      listen       8080;
      server_name  localhost;
      return 200 "hello from port 8080!\n";
  }
}
```
```bash
curl localhost:8080

# hello from port 8080!
```
... Or to `server_name`

```nginx
http {
  server {
      listen       80;
      server_name  example.com;

      return 200 "hello from example.com!\n";
  }
  server {
      listen       80;
      server_name  john.example.com;

      return 200 "hello from john.example.com!\n";
  }
}
```
```bash
curl http://john.example.com

# hello from john.example.com!
```

### Static Content

* Point to static content via the `root` directive (serve the `index.html` file by default)

```nginx
server {
  listen       80;
  server_name  localhost;
  root   /usr/src/app;
}
```
* Content-type :
`text/html html` -> parse any files with an `html` extension as `text/html`
```nginx
http {
  types {
      text/html html;
      text/css css;
  }

  server {
      listen 80;
      server_name  localhost;
      root   /usr/src/app;
  }
}
```
**If `types` is configured, only defined types are parsed !**